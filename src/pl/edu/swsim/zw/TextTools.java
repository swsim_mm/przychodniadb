package pl.edu.swsim.zw;

public class TextTools {
	
	public static final int lenDef = 45;
	
	/**
	 * Wypełnia String znakiem - do długości 90
	 * @param text
	 * @return
	 */
	public static String oneLine(String text) {
		return padText(text, "-");
	}

	/**
	 * Wypełnia String znakiem = do długości 90
	 * @param text
	 * @return
	 */
	public static String twoLine(String text) {
		return padText(text, "=");
	}
	
	/**
	 * Wypełnia String określonym znakiem do długości 90
	 * @param text
	 * @param sign
	 * @return
	 */
	public static String padText(String text, String sign) {
		return padText(text, sign, lenDef);
	}
	
	/**
	 * Wypełnia String określonym znakiem do określonej długości
	 * @param text
	 * @param sign
	 * @param len
	 * @return
	 */
	public static String padText(String text, String sign, int len) {
		if(text.length() > 0)
			text = " " + text + " ";
		if(sign.length() > 1)
			sign = sign.substring(0, 1);
		int padLength = (len - text.length()) / 2;
		String pad = "";
		for(int i = 0; i< padLength; i++)
			pad += sign;
		return pad + text + pad + (((len - text.length()) % 2 != 0) ? sign : "");
	}
	
	
	/**
	 * Zwraca ograniczony do maksymalnej długości łańcuch string
	 * null gdy podany null w parametrze
	 * @param inputString
	 * @param maxLength
	 * @return
	 */
	public static String limitString(String inputString, int maxLength) {

		if(inputString == null)
			return null;
		
		if(maxLength < 0) 
			maxLength = 0;
		
		int realMaxLength = inputString.length();
		
		if (inputString.length() > maxLength) 
			realMaxLength = maxLength;
		
		return inputString.substring(0, realMaxLength);
	}
}
