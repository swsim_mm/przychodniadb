package pl.edu.swsim.zw;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Klasa dostępowa do hibernate
 * @author Michał
 *
 */
public class HibernateFactory {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	/**
	 * Statyczne uruchomienie hibernate
	 */
	static {		
		logger.trace("Uruchomienie hibernate");
		// ukrycie masy ifnormacji od Hibernate
	    @SuppressWarnings("unused")
		org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
	    java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.WARNING); //or whatever level you need    
		makeConnection();
	}

	/**
	 * Nawiązanie połaczenia z bazą danych
	 * Informacje konfiguracyjne takie jak klasy oraz wpisy 
	 * konfiguracji są w xml w projekcie PrzychodniaDB
	 * (nie trzeba powtarzać konfiguracji)
	 */
	private static void makeConnection() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure();
			File file = null;
			try {
				logger.trace(TextTools.twoLine("Path"));
				logger.trace(new Object() { }.getClass().getEnclosingClass().getResource("").getPath());
				
				logger.trace(TextTools.twoLine("Ścieżka"));
				logger.trace(new Object() { }.getClass().getEnclosingClass().getResource("../../../../hibernate.cfg.xml").getPath());
				
				logger.trace(TextTools.twoLine(""));
				// workaround dostęp z innego projektu
				file = new File(new Object() { }.getClass().getEnclosingClass().getResource("../../../../hibernate.cfg.xml").getPath());
				
			} catch(Exception e) {
				logger.trace(TextTools.twoLine("Błąd pliku konfiguracji"));
				logger.trace("Nie znaleziono pliku hibernate.cfg.xml");
				System.exit(1);
			}
			
		    configuration.configure(file);
		    configuration.setProperty("hbm2ddl.auto", "validate");
		    
		    //configuration.setProperty("connection.provider_class", "org.hibernate.c3p0.internal.C3P0ConnectionProvider");
			
			//serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		} catch (HibernateException he) {
			logger.fatal("Nie można uruchomić hibernate!");
			logger.fatal("Prawdopodobnie coś nie tak z konfiguracją lub serwer nieodpowiada");
			logger.fatal(he.getCause().getMessage());
		}
		catch (Exception e) {
			logger.fatal("Nie można uruchomić hibernate, nieznana przyczyna");
			e.printStackTrace();
		}

	}
	
	/**
	 * Szybko pobiera aktualną sejsę sesję bez try-cath
	 * @return null gdy nie da się utworzyć połaczenia
	 */
	public static Session getSession() {
		Session session = null; 
		
			try {
				session = sessionFactory.getCurrentSession();
				logger.trace(".hib ex");
			} catch (HibernateException e) {
				logger.trace(".hib 1");
				try {
					session = sessionFactory.openSession();
				} catch (HibernateException e1) {
					logger.fatal(".hib 2");
				} catch (Exception e1) {
					logger.fatal("Utworzenie nowej sesji nie udało się! Nieznana przyczyna");
					e1.printStackTrace();
				}
				
			} catch (Exception e) {
				logger.fatal("Coś nie tak z Hibernate !");
				e.printStackTrace();
			}

		return session;
	}

	/**
	 * Umożliwia poprawnie zamykania aplikacji, poprzez poprawnie 
	 * zwolnienie zasobów zamist system.exit 
	 */
	public static void close() {
		sessionFactory.close();
	}

}