package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "pacjenci")
public class PacjentEnt implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String imie;
	
	@Column(length = 64)
	private String nazwisko;
	
	@Column(length = 11)
	private String pesel;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ulicaid")
	private UlicaEnt ulica;
	
	@Column(length = 10)
	private String budynek;
	
	@Column(length = 10)
	private String mieszkanie;
	
	@Column(length = 30)
	private String telefon;
		
	@Column(length = 255)
	private String karta_nfz;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pracownikid")
	private PracownikEnt pracownik;
	
	@Column(length = 1500)
	private String dodatkowe;
	
	@OneToMany(mappedBy = "pacjent", fetch = FetchType.LAZY)
    private Set<TerminEnt> wizyty;
	
	//--------------------------------------------------------------
	public PacjentEnt() { }
	
	public PacjentEnt(PacjentEnt pacjent) { 
		if(pacjent == null)
			return;
		this.id = pacjent.id;
		this.imie = pacjent.imie;
		this.nazwisko = pacjent.nazwisko;
		this.pesel = pacjent.pesel;
		this.ulica = new UlicaEnt(pacjent.ulica);
		this.budynek = pacjent.budynek;
		this.mieszkanie = pacjent.mieszkanie;
		this.telefon = pacjent.telefon;
		this.pracownik = new PracownikEnt(pacjent.pracownik);
		this.karta_nfz = pacjent.karta_nfz;
		this.dodatkowe = pacjent.dodatkowe;		
	}
	//--------------------------------------------------------------
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = TextTools.limitString(imie, 64);
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = TextTools.limitString(nazwisko, 64);
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = TextTools.limitString(pesel, 11);
	}

	public UlicaEnt getUlica() {
		return ulica;
	}

	public void setUlica(UlicaEnt ulica) {
		this.ulica = ulica;
	}

	public String getBudynek() {
		return budynek;
	}

	public void setBudynek(String budynek) {
		this.budynek = TextTools.limitString(budynek, 10);
	}

	public String getMieszkanie() {
		return mieszkanie;
	}

	public void setMieszkanie(String mieszkanie) {
		this.mieszkanie = TextTools.limitString(mieszkanie, 10);
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = TextTools.limitString(telefon, 30);
	}

	public String getKarta_nfz() {
		return karta_nfz;
	}

	public void setKarta_nfz(String karta_nfz) {
		this.karta_nfz = TextTools.limitString(karta_nfz, 255);
	}

	public PracownikEnt getPracownik() {
		return pracownik;
	}

	public void setPracownik(PracownikEnt pracownik) {
		this.pracownik = pracownik;
	}

	public String getDodatkowe() {
		return dodatkowe;
	}

	public void setDodatkowe(String dodatkowe) {
		this.dodatkowe = TextTools.limitString(dodatkowe, 1500);
	}

	public Set<TerminEnt> getWizyty() {
		return wizyty;
	}

	public void setWizyty(Set<TerminEnt> wizyty) {
		this.wizyty = wizyty;
	}
//----------------------------------------------------
	public String toString() {
		
		return "PacjentEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"imie: " + this.imie + System.lineSeparator() +
				"nazwisko: " + this.nazwisko + System.lineSeparator() +
				"pesel: " + this.pesel + System.lineSeparator() +
				"budynek: " + this.budynek + System.lineSeparator() +
				"mieszkanie: " + this.mieszkanie + System.lineSeparator() +
				"telefon: " + this.telefon + System.lineSeparator() +
				"karta nfz: " + this.karta_nfz + System.lineSeparator() +
				"dodatkowe: " + this.dodatkowe;
	}
	
}