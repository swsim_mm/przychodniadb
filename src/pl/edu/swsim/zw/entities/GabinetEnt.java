package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "gabinety")
public class GabinetEnt implements Serializable  {
	
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String numer;
	
	@Column(length = 64)
	private String nazwa;
	
	@OneToMany(mappedBy = "gabinet", fetch = FetchType.LAZY)
    private Set<RodzajZabieguEnt> zabiegi;
	
	@OneToMany(mappedBy = "gabinet", fetch = FetchType.LAZY)
    private Set<RodzajWizytyEnt> wizyty;

	//----------------------------------------------------------------
	public GabinetEnt() { }
	
	public GabinetEnt(GabinetEnt gabinet) { 
		if(gabinet == null)
			return;
		this.id = gabinet.id;
		this.numer = gabinet.numer;
		this.nazwa = gabinet.nazwa;
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getNumer() {
		return numer;
	}

	public void setNumer(String numer) {
		this.numer = TextTools.limitString(numer, 64);
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public Set<RodzajZabieguEnt> getZabiegi() {
		return zabiegi;
	}

	public void setZabiegi(Set<RodzajZabieguEnt> zabiegi) {
		this.zabiegi = zabiegi;
	}

	public Set<RodzajWizytyEnt> getWizyty() {
		return wizyty;
	}

	public void setWizyty(Set<RodzajWizytyEnt> wizyty) {
		this.wizyty = wizyty;
	}
//-------------------------------------------------------
	public String toString() {
		
		return "GabinetEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"numer: " + this.numer + System.lineSeparator() +
				"nazwa: " + this.nazwa;
	}

	

}
