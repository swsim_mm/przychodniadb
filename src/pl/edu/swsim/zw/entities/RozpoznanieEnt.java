package pl.edu.swsim.zw.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "rozpoznania")
public class RozpoznanieEnt implements Serializable {
	
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
		
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "chorobaid")
	private ChorobaEnt choroba;	
		
	@Column(length = 1000)
	private String opis;
		
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "wizytaid")
	private TerminEnt wizyta;

	//----------------------------------------------------------------
	public RozpoznanieEnt() { }
	
	public RozpoznanieEnt(RozpoznanieEnt rozpoznanie) { 
		if(rozpoznanie == null)
			return;
		this.id = rozpoznanie.id;
		this.choroba = new ChorobaEnt(rozpoznanie.choroba);
		this.opis = rozpoznanie.opis;
		this.wizyta = new TerminEnt(rozpoznanie.wizyta);
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ChorobaEnt getChoroba() {
		return choroba;
	}

	public void setChoroba(ChorobaEnt choroba) {
		this.choroba = choroba;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = TextTools.limitString(opis, 1000);
	}


	public TerminEnt getWizyta() {
		return wizyta;
	}

	public void setWizyta(TerminEnt wizyta) {
		this.wizyta = wizyta;
	}
//------------------------------------------------------------
	public String toString() {
		
		return "RozpoznanieEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"opis: " + this.opis + System.lineSeparator();

	}


	

}
