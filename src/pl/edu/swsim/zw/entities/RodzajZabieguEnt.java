package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "rodzaje_zabiegow")
public class RodzajZabieguEnt implements Serializable  {
	
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String nazwa;
	
	@Column(length = 64)
	private String rodzaj;
	
	private int cena;
	
	private boolean refundacja;
	
	private int punkty;
	
	private boolean skierowanie;
	
	//@Column(columnDefinition="DATETIME")
	private java.sql.Time czastrwania;
	
	@Column(length = 1000)
	private String opis;
	
	@OneToMany(mappedBy = "rodzajzabiegu", fetch = FetchType.LAZY)
    private Set<TerminEnt> terminy;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "gabinetid")
	private GabinetEnt gabinet;	
	
	//----------------------------------------------------------------
	public RodzajZabieguEnt() { }
	
	public RodzajZabieguEnt(RodzajZabieguEnt rodzajZabiegu) { 
		if(rodzajZabiegu == null)
			return;
		this.id = rodzajZabiegu.id;
		this.nazwa = rodzajZabiegu.nazwa;
		this.rodzaj = rodzajZabiegu.rodzaj;
		this.cena = rodzajZabiegu.cena;
		this.gabinet = new GabinetEnt(rodzajZabiegu.getGabinet());
		this.refundacja = rodzajZabiegu.refundacja;
		this.punkty = rodzajZabiegu.punkty;
		this.skierowanie = rodzajZabiegu.skierowanie;
		this.czastrwania = rodzajZabiegu.czastrwania;
		this.opis = rodzajZabiegu.opis;
	}
	//----------------------------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public String getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(String rodzaj) {
		this.rodzaj = TextTools.limitString(rodzaj, 64);
	}

	/**
	 * cena wyliczona do 2 miejsc po przecinku
	 * @return
	 */
	public float getCena() {
		return (float)cena / 100;
	}

	/** 
	 * cena do 2 miejsc po przecinku
	 * @param cena
	 */
	public void setCena(float cena) {
		this.cena = (int)(cena * 100);
	}

	public boolean isRefundacja() {
		return refundacja;
	}

	public void setRefundacja(boolean refundacja) {
		this.refundacja = refundacja;
	}

	public int getPunkty() {
		return punkty;
	}

	public void setPunkty(int punkty) {
		this.punkty = punkty;
	}

	public boolean isSkierowanie() {
		return skierowanie;
	}

	public void setSkierowanie(boolean skierowanie) {
		this.skierowanie = skierowanie;
	}

	public java.sql.Time getCzastrwania() {
		return czastrwania;
	}
	
	public void setCzastrwania(java.sql.Time czastrwania) {
		this.czastrwania = czastrwania;
	}
	
	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = TextTools.limitString(opis, 1000);
	}

	public Set<TerminEnt> getTerminy() {
		return terminy;
	}

	public void setTerminy(Set<TerminEnt> terminy) {
		this.terminy = terminy;
	}

	public GabinetEnt getGabinet() {
		return gabinet;
	}

	public void setGabinet(GabinetEnt gabinet) {
		this.gabinet = gabinet;
	}
	//-----------------------------------------------------
	public String toString() {
		
		return "RodzajZabieguEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa + System.lineSeparator() +
				"rodzaj: " + this.rodzaj + System.lineSeparator() +
				//TODO - do sprawdzenia/poprawki
				//"cena: " + this.cena + System.lineSeparator() +
				"refundacja: " + (this.refundacja?"Tak":"Nie") + System.lineSeparator() +
				"punkty: " + Integer.toString(punkty) + System.lineSeparator() +
				"skierowanie: " + (this.skierowanie?"Tak":"Nie") + System.lineSeparator() +
				//"czastrwania: " + this.czastrwania + System.lineSeparator() +
				"opis:" + this.opis;
				
	}
}