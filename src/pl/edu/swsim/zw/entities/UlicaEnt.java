package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "ulice")
public class UlicaEnt implements Serializable {
	
	@Transient
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "miastoid")
	private MiastoEnt miasto;
	
	@Column(length = 64)
	private String nazwa;
	
	@OneToMany(mappedBy = "ulica", fetch = FetchType.LAZY)
    private Set<PacjentEnt> pacjenci;
	
	@OneToMany(mappedBy = "ulica")
    private Set<PracownikEnt> pracownicy;
	
	//----------------------------------------------------------------
	public UlicaEnt() {}
	
	public UlicaEnt(UlicaEnt ulica) {
		if(ulica == null)
			return;
		this.id = ulica.id;
		this.miasto = new MiastoEnt(ulica.miasto);
		this.nazwa = ulica.nazwa;
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MiastoEnt getMiasto() {
		return miasto;
	}

	public void setMiasto(MiastoEnt miasto) {
		this.miasto = miasto;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public Set<PacjentEnt> getPacjenci() {
		return pacjenci;
	}

	public void setPacjenci(Set<PacjentEnt> pacjenci) {
		this.pacjenci = pacjenci;
	}

	public Set<PracownikEnt> getPracownicy() {
		return pracownicy;
	}

	public void setPracownicy(Set<PracownikEnt> pracownicy) {
		this.pracownicy = pracownicy;
	}
	
	//----------------------------------------------------------------

	public String toString() {
		return "UlicaEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa;
	}

}
