package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "miasta")
public class MiastoEnt implements Serializable{
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "wojewodztwoid")
	private WojewodztwoEnt wojewodztwo;
	
	@Column(length = 64)
	private String nazwa;

	@Column(length = 6)
	private String kod_pocztowy;
	
	@OneToMany(mappedBy = "miasto")//, orphanRemoval = true)
    private Set<UlicaEnt> ulice;

	//----------------------------------------------------------------
	public MiastoEnt() {}
	
	public MiastoEnt(long id) {
		this.id = id;
	}
	
	public MiastoEnt(MiastoEnt miasto) {
		if(miasto == null)
			return;
		this.id = miasto.id;
		this.wojewodztwo = new WojewodztwoEnt(miasto.wojewodztwo);
		this.nazwa = miasto.nazwa;
		this.kod_pocztowy = miasto.kod_pocztowy;
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public WojewodztwoEnt getWojewodztwo() {
		return wojewodztwo;
	}

	public void setWojewodztwo(WojewodztwoEnt wojewodztwo) {
		this.wojewodztwo = wojewodztwo;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public String getKod_pocztowy() {
		return kod_pocztowy;
	}

	public void setKod_pocztowy(String kod_pocztowy) {
		this.kod_pocztowy = TextTools.limitString(kod_pocztowy, 6);
	}

	public Set<UlicaEnt> getUlice() {
		return ulice;
	}

	public void setUlice(Set<UlicaEnt> ulice) {
		this.ulice = ulice;
	}
	
	//-------------------------------------

	public String toString() {
		return "MiastoEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa + System.lineSeparator() +
				"kod_pocztowy" + this.kod_pocztowy;
	}
	
}