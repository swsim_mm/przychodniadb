package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "wojewodztwa")
public class WojewodztwoEnt implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String nazwa;
	
	@OneToMany(mappedBy = "wojewodztwo")
    private Set<MiastoEnt> miasta;

	//----------------------------------------------------------------
	public WojewodztwoEnt() { }
	
	public WojewodztwoEnt(long id) {
		this.id = id;
	}

	public WojewodztwoEnt(WojewodztwoEnt woj) {
		if(woj == null)
			return;
		this.id = woj.id;
		this.nazwa = woj.nazwa;
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public Set<MiastoEnt> getMiasta() {
		return miasta;
	}

	public void setMiasta(Set<MiastoEnt> miasta) {
		this.miasta = miasta;
	}
	
	public String toString() {
		return "WojewodztwoEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa;
	}

}
