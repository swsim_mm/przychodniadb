package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "choroby")
public class ChorobaEnt  implements Serializable {
	
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String nazwa;
	
	private boolean przewlekla;
	
	@Column(length = 1500)
	private String opis;
	
	@OneToMany(mappedBy = "choroba", fetch = FetchType.LAZY)
    private Set<RozpoznanieEnt> rozpoznania;
	
	//----------------------------------------------------------------
	public ChorobaEnt() {}
	
	public ChorobaEnt(ChorobaEnt choroba) {
		if(choroba == null)
			return;
		this.id = choroba.id;
		this.nazwa = choroba.nazwa;
		this.przewlekla = choroba.przewlekla;
		this.opis = choroba.opis;
	}
	//----------------------------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public boolean isPrzewlekla() {
		return przewlekla;
	}

	public void setPrzewlekla(boolean przewlekla) {
		this.przewlekla = przewlekla;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = TextTools.limitString(opis, 1500);
	}

	public Set<RozpoznanieEnt> getRozpoznania() {
		return rozpoznania;
	}

	public void setRozpoznania(Set<RozpoznanieEnt> rozpoznania) {
		this.rozpoznania = rozpoznania;
	}
//--------------------------------------------------
	public String toString() {
		return "ChorobaEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa  + System.lineSeparator() +
				"przewlekla: " + (this.przewlekla?"Tak":"Nie") + System.lineSeparator() +
				"opis :" + this.opis;
	}
}
