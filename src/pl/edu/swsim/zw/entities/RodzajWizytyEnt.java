package pl.edu.swsim.zw.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "rodzaje_wizyt")
public class RodzajWizytyEnt {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String nazwa;
	
	//@Column(columnDefinition="DATETIME")
	private java.sql.Time czastrwania;
	
	@OneToMany(mappedBy = "rodzajwizyty", fetch = FetchType.LAZY)
    private Set<TerminEnt> terminy;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "gabinetid")
	private GabinetEnt gabinet;	

	//----------------------------------------------------------------
	public RodzajWizytyEnt() { }
	
	public RodzajWizytyEnt(RodzajWizytyEnt rodzajWizyty) { 
		if(rodzajWizyty == null)
			return;
		this.id = rodzajWizyty.id;
		this.nazwa = rodzajWizyty.nazwa;
		this.gabinet = new GabinetEnt(rodzajWizyty.getGabinet());
	}
	//----------------------------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = TextTools.limitString(nazwa, 64);
	}

	public java.sql.Time getCzastrwania() {
		return czastrwania;
	}

	public void setCzastrwania(java.sql.Time czastrwania) {
		this.czastrwania = czastrwania;
	}

	public Set<TerminEnt> getTerminy() {
		return terminy;
	}

	public void setTerminy(Set<TerminEnt> terminy) {
		this.terminy = terminy;
	}

	public GabinetEnt getGabinet() {
		return gabinet;
	}

	public void setGabinet(GabinetEnt gabinet) {
		this.gabinet = gabinet;
	}
//--------------------------------------------------------
	public String toString() {
		return "RodzajWizytyEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nazwa: " + this.nazwa;
	}

}
