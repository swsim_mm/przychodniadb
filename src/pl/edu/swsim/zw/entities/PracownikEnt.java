package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "pracownicy")
public class PracownikEnt implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	@Transient
	public static final int TYP_LEKARZ = 1;
	@Transient
	public static final int TYP_INNY = 0;
	@Transient
	public static final int TYP_DOWOLNY = -1;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length = 64)
	private String imie;
	
	@Column(length = 64)
	private String nazwisko;
	
	@Column(length = 11)
	private String pesel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ulicaid")
	private UlicaEnt ulica;
	
	@Column(length = 10)
	private String budynek;
	
	@Column(length = 10)
	private String mieszkanie;
	
	@Column(length = 18)
	private String telefon;
		
	@Column(length = 1000)
	private String dodatkowe;
	
	@Column(length = 20)
	private String pwz; //prawo wykonywania zawodu
	
	private boolean lekarz;
	
	@OneToMany(mappedBy = "pracownik")
    private Set<PacjentEnt> pacjenci;
	
	@OneToMany(mappedBy = "pracownik")
    private Set<TerminEnt> wizyty;
		
	@OneToMany(mappedBy = "pracownik")
    private Set<UzytkownikEnt> uzytkownik;
		
	// Mapowanie ManyToMany z dodatkową tabelą
	/*@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "grupy_pracownicy", 
			joinColumns = { @JoinColumn(name = "pracownikid") }, 
			inverseJoinColumns = { @JoinColumn(name = "grupaid") })
    private Set<GrupaEnt> grupy;
*/
	//----------------------------------------------------------------
	public PracownikEnt() { }
	
	public PracownikEnt(PracownikEnt pracownik) { 
		if(pracownik == null)
			return;
		this.id = pracownik.id;
		this.imie = pracownik.imie;
		this.nazwisko = pracownik.nazwisko;
		this.pesel = pracownik.pesel;
		this.ulica = new UlicaEnt(pracownik.ulica);
		this.budynek = pracownik.budynek;
		this.mieszkanie = pracownik.mieszkanie;
		this.telefon = pracownik.telefon;
		this.dodatkowe = pracownik.dodatkowe;
		this.pwz = pracownik.pwz;
		this.lekarz = pracownik.lekarz;
	}
	//----------------------------------------------------------------
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = TextTools.limitString(imie, 64);
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = TextTools.limitString(nazwisko, 64);
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = TextTools.limitString(pesel, 64);
	}

	public UlicaEnt getUlica() {
		return ulica;
	}

	public void setUlica(UlicaEnt ulica) {
		this.ulica = ulica;
	}

	public String getBudynek() {
		return budynek;
	}

	public void setBudynek(String budynek) {
		this.budynek = TextTools.limitString(budynek, 10);
	}

	public String getMieszkanie() {
		return mieszkanie;
	}

	public void setMieszkanie(String mieszkanie) {
		this.mieszkanie = TextTools.limitString(mieszkanie, 10);
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = TextTools.limitString(telefon, 18);
	}

	public String getDodatkowe() {
		return dodatkowe;
	}

	public void setDodatkowe(String dodatkowe) {
		this.dodatkowe = TextTools.limitString(dodatkowe, 1000);
	}

	public String getPwz() {
		return pwz;
	}

	public void setPwz(String pwz) {
		this.pwz = pwz;
	}
	
	public boolean isLekarz() {
		return lekarz;
	}

	public void setLekarz(boolean lekarz) {
		this.lekarz = lekarz;
	}

	public Set<PacjentEnt> getPacjenci() {
		return pacjenci;
	}

	public void setPacjenci(Set<PacjentEnt> pacjenci) {
		this.pacjenci = pacjenci;
	}

	public Set<TerminEnt> getWizyty() {
		return wizyty;
	}

	public void setWizyty(Set<TerminEnt> wizyty) {
		this.wizyty = wizyty;
	}

	public Set<UzytkownikEnt> getUzytkownik() {
		return uzytkownik;
	}

	public void setUzytkownik(Set<UzytkownikEnt> uzytkownik) {
		this.uzytkownik = uzytkownik;
	}

	public String toString() {
		
		return "PracownikEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"imie: " + this.imie + System.lineSeparator() +
				"nazwisko:" + this.nazwisko + System.lineSeparator() +
				"pesel:" + this.pesel + System.lineSeparator() +
				"budynek:" + this.budynek + System.lineSeparator() +
				"mieszkanie:" + this.mieszkanie + System.lineSeparator() +
				"telefon:" + this.telefon + System.lineSeparator() +
				"dodatkowe:" + this.dodatkowe + System.lineSeparator() +
				"pwz:" + this.pwz + System.lineSeparator() +
				"lekarz:" + (this.lekarz?"Tak":"Nie");
	}
}
