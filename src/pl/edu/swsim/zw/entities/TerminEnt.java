package pl.edu.swsim.zw.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import pl.edu.swsim.zw.TextTools;

@Entity
@Table(name = "terminy")
public class TerminEnt implements Serializable {
	
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pacjentid")
	private PacjentEnt pacjent;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pracownikid")
	private PracownikEnt pracownik;	
	
	private boolean zabieg;
	
	/*@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "rodzajwizytyid", nullable = true)
	private RodzajWizytyEnt rodzajwizyty;	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "rodzajzabieguid", nullable = true)
	private RodzajZabieguEnt rodzajzabiegu;	*/
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rodzajwizytyid", nullable = true)
	private RodzajWizytyEnt rodzajwizyty;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rodzajzabieguid", nullable = true)
	private RodzajZabieguEnt rodzajzabiegu;	

	private boolean nfz;
	
	@Column(columnDefinition="DATETIME")
	private Date termin;
	
	@Column(length = 1000)
	private String dodatkowe;
	
	private boolean obecnosc = false;
		
	@OneToMany(mappedBy = "wizyta", fetch = FetchType.LAZY)
    private Set<RozpoznanieEnt> rozpoznania;
	
	//----------------------------------------------------------------
	public TerminEnt() { }
	
	public TerminEnt(TerminEnt wizyta) { 
		if(wizyta == null)
			return;
		this.id = wizyta.id;
		this.pacjent = new PacjentEnt(wizyta.pacjent);
		this.pracownik = new PracownikEnt(wizyta.pracownik);
		this.rodzajwizyty = new RodzajWizytyEnt(wizyta.rodzajwizyty);
		this.nfz = wizyta.nfz;
		this.termin = new java.sql.Timestamp(wizyta.termin.getTime());
		this.dodatkowe = wizyta.dodatkowe;
		this.obecnosc = wizyta.obecnosc;
	}
	//----------------------------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PacjentEnt getPacjent() {
		return pacjent;
	}

	public void setPacjent(PacjentEnt pacjent) {
		this.pacjent = pacjent;
	}

	public PracownikEnt getPracownik() {
		return pracownik;
	}

	public void setPracownik(PracownikEnt pracownik) {
		this.pracownik = pracownik;
	}

	public RodzajWizytyEnt getRodzajwizyty() {
		return rodzajwizyty;
	}

	public void setRodzajwizyty(RodzajWizytyEnt rodzajwizyty) {
		this.rodzajwizyty = rodzajwizyty;
	}

	public boolean isNfz() {
		return nfz;
	}

	public void setNfz(boolean nfz) {
		this.nfz = nfz;
	}

	public Date getTermin() {
		return termin;
	}

	public void setTermin(Date termin) {
		this.termin = termin;
	}

	public String getDodatkowe() {
		return dodatkowe;
	}

	public void setDodatkowe(String dodatkowe) {
		this.dodatkowe = TextTools.limitString(dodatkowe, 1000);
	}

	public boolean isObecnosc() {
		return obecnosc;
	}

	public void setObecnosc(boolean obecnosc) {
		this.obecnosc = obecnosc;
	}

	public Set<RozpoznanieEnt> getRozpoznania() {
		return rozpoznania;
	}

	public void setRozpoznania(Set<RozpoznanieEnt> rozpoznania) {
		this.rozpoznania = rozpoznania;
	}

	public boolean isZabieg() {
		return zabieg;
	}

	public void setZabieg(boolean zabieg) {
		this.zabieg = zabieg;
	}

	public RodzajZabieguEnt getRodzajzabiegu() {
		return rodzajzabiegu;
	}

	public void setRodzajzabiegu(RodzajZabieguEnt rodzajzabiegu) {
		this.rodzajzabiegu = rodzajzabiegu;
	}
//------------------------------------------------------------------
	public String toString() {
		
		return "TerminEnt" + System.lineSeparator() +
				"id: " + Long.toString(id) + System.lineSeparator() +
				"nfz: " + (this.nfz?"Tak":"Nie") + System.lineSeparator() +
				//TODO
				//"termin: " + this.termin + System.lineSeparator() +
				"dodatkowe: " + this.dodatkowe + System.lineSeparator() +
				"obecnosc: " + (this.obecnosc?"Tak":"Nie");
	}

}