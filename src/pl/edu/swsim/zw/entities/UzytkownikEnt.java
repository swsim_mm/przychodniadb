package pl.edu.swsim.zw.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;

import pl.edu.swsim.zw.TextTools;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "uzytkownicy")
public class UzytkownikEnt implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pracownikid")
	private PracownikEnt pracownik;
	
	@Column(length = 64)
	private String login;
	
	@Column(length = 64)
	private String haslo;
	
	private boolean administrator;
	
	//TODO to też? jak coś, to zmienione -m.w
	@Index(name = "session_index") @Column(length=32)
	private String session;

	@Column(columnDefinition="DATETIME")
	private java.sql.Timestamp last_visit;
	
	@Column(columnDefinition="DATETIME")
	private java.sql.Timestamp timeout_visit;
	
	// Mapowanie ManyToMany z dodatkową tabelą
	/*@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "acl_uzytkownicy", 
			joinColumns = { @JoinColumn(name = "uzytkownikid") }, 
			inverseJoinColumns = { @JoinColumn(name = "aclid") })
    private Set<AclEnt> acl;*/
	
	//----------------------------------------------
	
	public UzytkownikEnt() {}
	
	public UzytkownikEnt(UzytkownikEnt uzytkownik) {
		if(uzytkownik == null)
			return;
		this.id = uzytkownik.id;
		this.pracownik = new PracownikEnt(uzytkownik.pracownik);
		this.login = uzytkownik.login;
		this.haslo = uzytkownik.haslo;
		this.session = uzytkownik.session;
		this.last_visit = uzytkownik.last_visit;
		this.timeout_visit = uzytkownik.timeout_visit;
	}
	
	//----------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PracownikEnt getPracownik() {
		return pracownik;
	}

	public void setPracownik(PracownikEnt pracownik) {
		this.pracownik = pracownik;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = TextTools.limitString(login, 64);
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String haslo) {
		this.haslo = TextTools.limitString(haslo, 64);
	}

	/*public Set<AclEnt> getAcl() {
		return acl;
	}

	public void setAcl(Set<AclEnt> acl) {
		this.acl = acl;
	}*/

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = TextTools.limitString(session, 32);
	}

	public java.sql.Timestamp getLast_visit() {
		return last_visit;
	}

	public void setLast_visit(java.sql.Timestamp last_visit) {
		this.last_visit = last_visit;
	}

	public java.sql.Timestamp getTimeout_visit() {
		return timeout_visit;
	}

	public void setTimeout_visit(java.sql.Timestamp timeout_visit) {
		this.timeout_visit = timeout_visit;
	}
	
	
	
	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public String toString() {

		return "UżytkownikEnt" 
				+ ", id:" + this.id 
				+ ", login:" + this.login 
				+ ", haslo:" + this.haslo 
				+ ", session:" + this.session 
				+ ", " ;
	}
	
	
}
