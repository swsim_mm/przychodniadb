package pl.edu.swsim.zw;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DbCreate {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());

	public static void main(String[] args) throws InterruptedException {
		logger.info(".");
		
		logger.info(TextTools.oneLine(""));
		logger.info(TextTools.oneLine("Odtwarzanie bazy"));
		logger.info(TextTools.oneLine(""));
		logger.info("Operacja do wykonania");
		logger.info(TextTools.oneLine(""));
		logger.info("1 - tylko aktualizacja");
		logger.info("2 - pełen reset");
		logger.info("inny znak - anuluj");
		logger.info(TextTools.twoLine(""));
		
		Scanner in = new Scanner(System.in);
		String s = in.next();
		in.close();
		
		if(s.toLowerCase().compareTo("1") == 0 ) {
			logger.info(TextTools.oneLine("Odtwarzanie"));
			Thread.sleep(1000);
			reCreateDatabase();
			logger.info(TextTools.oneLine("KONIEC"));
		} else if (s.toLowerCase().compareTo("2") == 0 ) {
			logger.info(TextTools.twoLine("Usuwanie z kluczami obcymi"));
			Thread.sleep(1000);
			clearDatabase();
			logger.info(TextTools.oneLine("Zakończono"));
			
			logger.info(TextTools.twoLine("Odtwarzanie"));
			Thread.sleep(1000);
			reCreateDatabase();
			logger.info(TextTools.oneLine("Zakończono"));
		} else {
			logger.info(TextTools.padText("Anulowanie", "."));
		}
	}
	
	public static void reCreateDatabase() {
		
		logger.info(TextTools.oneLine("Tworzenie konfiguracji"));
		Configuration configuration = new Configuration();

		
		File file = new File(new Object() { }.getClass().getEnclosingClass().getResource("../../../../hibernate.cfg.xml").getPath());
		
	    configuration.configure(file);
	    configuration.setProperty("hibernate.hbm2ddl.auto", "create");
	    configuration.setProperty("hibernate.hbm2ddl.import_files", "import.sql");
	    //configuration.configure();
		
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
		
		logger.info(TextTools.oneLine("Tworzenie factory"));
		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
		Session session = sessionFactory.openSession();
		
		if(session.isConnected()) {
			logger.info(TextTools.twoLine("Próba połaczenia powiodła się!"));
		} else {
			logger.error(TextTools.twoLine("Próba połaczenia niepowiodła się!"));
		}
		
		logger.info(TextTools.oneLine("Kończenie odtwarzania"));
		
		session.close();
		sessionFactory.close();
		
	}
	
	@SuppressWarnings("unchecked")
	public static void clearDatabase() {
		logger.info(TextTools.oneLine("Tworzenie konfiguracji"));
		Configuration configuration = new Configuration();
		Configuration dirtyConfiguration = new Configuration();
		
		File file = new File(new Object() { }.getClass().getEnclosingClass().getResource("../../../../hibernate.cfg.xml").getPath());
		
		dirtyConfiguration.configure(file);
		
		ArrayList<String> propertyList = new ArrayList<String>();
		propertyList.add("hibernate.connection.driver_class");
		propertyList.add("hibernate.dialect");
		
		propertyList.add("hibernate.connection.url");
		propertyList.add("hibernate.connection.username");
		propertyList.add("hibernate.connection.password");
		
		propertyList.add("hibernate.connection.CharSet");
		propertyList.add("hibernate.connection.characterEncoding");
		propertyList.add("hibernate.connection.useUnicode");
		
		logger.info(TextTools.oneLine("\"Kradnięcie\" tylko wymaganych wartości..."));
		for(String prop: propertyList) {
			configuration.setProperty(prop, dirtyConfiguration.getProperty(prop));
		}

		configuration.setProperty("show_sql", "true");
		configuration.setProperty("format_sql", "true");
		

		
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
		
		logger.info(TextTools.oneLine("Tworzenie factory"));
		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
		Session session = sessionFactory.openSession();
		
		if(session.isConnected()) {
			logger.error(TextTools.twoLine("Próba połaczenia powiodła się!"));
		} else {
			logger.error(TextTools.twoLine("Próba połaczenia niepowiodła się!"));
		}
		
		SQLQuery q1 = session.createSQLQuery("SET FOREIGN_KEY_CHECKS=0;");
		q1.executeUpdate();
		
		SQLQuery q2 = session.createSQLQuery(
				"SELECT Concat('DROP TABLE ',table_schema,'.',TABLE_NAME, ';') "
				+ "FROM INFORMATION_SCHEMA.TABLES where table_schema in ('przychodnia');");
		List<?> list = q2.list();
		
		for(String str: (List<String>)list)
			session.createSQLQuery(str).executeUpdate();
		
		SQLQuery q3 = session.createSQLQuery("SET FOREIGN_KEY_CHECKS=1;");
		q3.executeUpdate();
		
		logger.info(TextTools.oneLine("Kończenie działania"));
		
		session.close();
		sessionFactory.close();
	}

}
