package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

public class RodzajeZabiegowRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private RodzajZabieguEnt rodzajZabiegu = null;
	private String SearchText = null;
	
	
	public RodzajeZabiegowRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public RodzajeZabiegowRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public RodzajeZabiegowRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public RodzajeZabiegowRep setId(long id) {
		this.id = id;
		return this;
	}
	public RodzajeZabiegowRep setRodzajZabiegu(RodzajZabieguEnt rodzajZabiegu) {
		this.rodzajZabiegu = rodzajZabiegu;
		return this;
	}
	public RodzajeZabiegowRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	} 
	
	/**
	 * 
	 */
	public RodzajeZabiegowRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public RodzajeZabiegowRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	
	/**
	 * Dodaje nowy RodzajZabiegu
	 * @return
	 */
	public RodzajeZabiegowRep AddRodzajZabiegu() {
		logger.trace(".");
			
		if(this.rodzajZabiegu == null) 
			throw new NullPointerException("Rodzaj Zabiegu nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
				
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(rodzajZabiegu);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edycja RodzajZabiegu
	 * @return
	 */
	public RodzajeZabiegowRep EditRodzajZabiegu() {
		logger.trace(".");
		
		if(this.rodzajZabiegu == null) 
			throw new NullPointerException("Rodzaj Zabiegu nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.rodzajZabiegu = (RodzajZabieguEnt) session.merge(this.rodzajZabiegu);
		}
		catch (Exception e) {
			logger.trace("merge fail");
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		try {
			session.update(rodzajZabiegu);
		} catch(Exception e) {
			logger.trace("fail to update");
		}

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	/**
	 * Usuwa RodzajZabiegu
	 * @return
	 */
	public RodzajeZabiegowRep DeleteRodzajZabiegu() {
		logger.trace(".");
		
		if(this.rodzajZabiegu == null) 
			throw new NullPointerException("Rodzaj Zabiegu nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			this.rodzajZabiegu = (RodzajZabieguEnt) session.merge(this.rodzajZabiegu);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(rodzajZabiegu);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<RodzajZabieguEnt> getRodzajeZabiegow() {
		
		logger.info(".");
		ArrayList<RodzajZabieguEnt> allRodzajeZabiegow = new ArrayList<RodzajZabieguEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allRodzajeZabiegow;
		
		Criteria criteria = session.createCriteria(RodzajZabieguEnt.class);
						
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("opis", "%" + this.SearchText + "%"))
					.add(Restrictions.like("rodzaj", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allRodzajeZabiegow = (ArrayList<RodzajZabieguEnt>) res;
		
		return allRodzajeZabiegow;
	}
	
}
