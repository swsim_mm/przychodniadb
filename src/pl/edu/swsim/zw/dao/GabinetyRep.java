package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.GabinetEnt;

public class GabinetyRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private GabinetEnt gabinet = null;
	private String SearchText = null;
	
	
	public GabinetyRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public GabinetyRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public GabinetyRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public GabinetyRep setId(long id) {
		this.id = id;
		return this;
	}
	public GabinetyRep setGabinet(GabinetEnt gabinet) {
		this.gabinet = gabinet;
		return this;
	}
	public GabinetyRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	} 
	
	/**
	 * 
	 */
	public GabinetyRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public GabinetyRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	
	/**
	 * Dodaje nowy gabinet
	 * @return
	 */
	public GabinetyRep AddGabinet() {
		logger.trace(".");
			
		if(this.gabinet == null) 
			throw new NullPointerException("gabinet nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
			
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(gabinet);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edycja gabinet
	 * @return
	 */
	public GabinetyRep EditGabinet() {
		logger.trace(".");
		
		if(this.gabinet == null) 
			throw new NullPointerException("gabinet nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.gabinet = (GabinetEnt) session.merge(this.gabinet);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(gabinet);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	/**
	 * Usuwa gabinet
	 * @return
	 */
	public GabinetyRep DeleteGabinet() {
		logger.trace(".");
		
		if(this.gabinet == null) 
			throw new NullPointerException("gabinet nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			this.gabinet = (GabinetEnt) session.merge(this.gabinet);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(gabinet);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<GabinetEnt> getGabinety() {
		
		logger.info(".");
		ArrayList<GabinetEnt> allGabinety = new ArrayList<GabinetEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allGabinety;
		
		Criteria criteria = session.createCriteria(GabinetEnt.class);
						
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("numer", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allGabinety = (ArrayList<GabinetEnt>) res;
		
		return allGabinety;
	}
	
}
