package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

public class TerminyRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private TerminEnt termin = null;
	private String SearchText = null; 
	
	private RodzajWizytyEnt rodzajWizyty = null;
	private RodzajZabieguEnt rodzajZabiegu = null;
	private PracownikEnt lekarz = null;
	private PacjentEnt pacjent = null;
	
	private Date startDate = null;
	private Date endDate = null;
	
	
	public static final int TYP_DOWOLNY = -1;
	public static final int TYP_WIZYTA = 1;
	public static final int TYP_ZABIEG = 2;
	
	private int typTerminu = TYP_DOWOLNY;

	public TerminyRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public TerminyRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public TerminyRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public TerminyRep setId(long id) {
		this.id = id;
		return this;
	}
	public TerminyRep setTermin(TerminEnt termin) {
		this.termin = termin;
		return this;
	}
	public TerminyRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}
	
	public TerminyRep setTypTerminu(int typTerminu) {
		this.typTerminu = typTerminu;
		return this;
	}
	public TerminyRep setRodzajWizyty(RodzajWizytyEnt rodzajWizyty) {
		this.rodzajWizyty = rodzajWizyty;
		return this;
	}
	public TerminyRep setRodzajZabiegu(RodzajZabieguEnt rodzajZabiegu) {
		this.rodzajZabiegu = rodzajZabiegu;
		return this;
	}
	public TerminyRep setLekarz(PracownikEnt lekarz) {
		this.lekarz = lekarz;
		return this;
	}
	public TerminyRep setPacjent(PacjentEnt pacjent) {
		this.pacjent = pacjent;
		return this;
	}
	public TerminyRep setStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}
	public TerminyRep setEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}
	public TerminyRep setThatDate(Date thatDate) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(thatDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		
		this.startDate = new Date();
		this.startDate.setTime(cal.getTimeInMillis());
		
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		
		this.endDate = new Date();
		this.endDate.setTime(cal.getTimeInMillis());
		
		return this;
	}
	
	/**
	 * 
	 */
	public TerminyRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public TerminyRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * Dodaje nowy Wizyta
	 * @return
	 */
	public TerminyRep AddTermin() {
		logger.trace(".");
			
		if(this.termin == null) 
			throw new NullPointerException("termin nie został zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
	
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(termin);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edytuje Wizyta
	 * @return
	 */
	public TerminyRep EditTermin() {
		logger.trace(".");
		
		if(this.termin == null) 
			throw new NullPointerException("termin nie został zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.termin = (TerminEnt) session.merge(this.termin);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(termin);
		
		if(autoTrasaction) 
			this.transaction.commit();

		return this;	
	}
	/**
	 * Usuń Wizyta
	 * @return
	 */
	public TerminyRep DeleteTermin() {
		logger.trace(".");
		
		if(this.termin == null) 
			throw new NullPointerException("termin nie został zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.termin = (TerminEnt) session.merge(this.termin);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(termin);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<TerminEnt> getTerminy() {
		
		logger.info(".");
		ArrayList<TerminEnt> allTerminy = new ArrayList<TerminEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allTerminy;
		
		Criteria criteria = session.createCriteria(TerminEnt.class);
		criteria.createAlias("pacjent", "pa");
		criteria.createAlias("pracownik", "pr");
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		//---
		if(this.rodzajWizyty != null) {
			criteria.createAlias("rodzajwizyty", "wiz");
			criteria.add(Restrictions.eq("wiz.gabinet.id", this.rodzajWizyty.getGabinet().getId()));
		}
		
		if(this.rodzajZabiegu != null) {
			criteria.createAlias("rodzajzabiegu", "zab");
			criteria.add(Restrictions.eq("zab.gabinet.id", this.rodzajZabiegu.getGabinet().getId()));
		}
		
		if(this.lekarz != null) {
			criteria.add(Restrictions.eq("pr.id", this.lekarz.getId()));
		}
		
		if(this.pacjent != null) {
			criteria.add(Restrictions.eq("pa.id", this.pacjent.getId()));
		}
		
		if(this.startDate != null)
			criteria.add(Restrictions.gt("termin", new java.sql.Date(this.startDate.getTime())));
		
		if(this.endDate != null)
			criteria.add(Restrictions.lt("termin", new java.sql.Date(this.endDate.getTime())));

		if(this.typTerminu > TYP_DOWOLNY) {
			if(this.typTerminu == TYP_WIZYTA) {
				criteria.add(Restrictions.eq("zabieg", false));
			} else if (this.typTerminu == TYP_ZABIEG) {
				criteria.add(Restrictions.eq("zabieg", true));
			}		
		}
		//---
		
		if(this.SearchText != null) {

			
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("dodatkowe", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("pa.imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.pesel", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.nazwisko", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("pr.imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pr.nazwisko", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allTerminy = (ArrayList<TerminEnt>) res;
		
		return allTerminy;
	}


}
