package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.ChorobaEnt;

public class ChorobyRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private ChorobaEnt choroba = null;
	private String SearchText = null;
	

	public ChorobyRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public ChorobyRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public ChorobyRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public ChorobyRep setId(long id) {
		this.id = id;
		return this;
	}
	public ChorobyRep setChoroba(ChorobaEnt choroba) {
		this.choroba = choroba;
		return this;
	}
	public ChorobyRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}

	/**
	 * 
	 */
	public ChorobyRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public ChorobyRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * Dodaje chorobę
	 * @return
	 */
	public ChorobyRep AddChoroba() {
		logger.trace(".");
		
		if(this.choroba == null) 
			throw new NullPointerException("choroba nie została zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
				
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		this.session.saveOrUpdate(this.choroba);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edytuje chorobę
	 * @return
	 */
	public ChorobyRep EditChoroba() {
		logger.trace(".");
		
		if(this.choroba == null) 
			throw new NullPointerException("choroba nie została zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.choroba = (ChorobaEnt) session.merge(this.choroba);
		} catch (Exception e) {	}
				
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		this.session.update(this.choroba);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	
	/**
	 * usuwa chorobę
	 * @return
	 */
	public ChorobyRep DeleteChoroba() {
		logger.trace(".");
		
		if(this.choroba == null) 
			throw new NullPointerException("choroba nie została zdefiniowana");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.choroba = (ChorobaEnt) session.merge(this.choroba);
		} catch (Exception e) {	}
			
		long itemID = choroba.getId();
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		this.session.delete(this.choroba);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		ArrayList<ChorobaEnt> items = this.setId(itemID).getChoroby();
		if(items.size() != 0)
			return null;
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ChorobaEnt> getChoroby() {
		
		logger.info(".");
		ArrayList<ChorobaEnt> allChoroby = new ArrayList<ChorobaEnt>();
		
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			return allChoroby;
		
		Criteria criteria = this.session.createCriteria(ChorobaEnt.class);
						
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("opis", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allChoroby = (ArrayList<ChorobaEnt>) res;
		
		return allChoroby;
	}


}
