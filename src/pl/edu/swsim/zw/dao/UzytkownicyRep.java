package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.entities.UzytkownikEnt;

public class UzytkownicyRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private UzytkownikEnt uzytkownik = null;
	private String SearchText = null;
	
	
	public UzytkownicyRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public UzytkownicyRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public UzytkownicyRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public UzytkownicyRep setId(long id) {
		this.id = id;
		return this;
	}
	public UzytkownicyRep setUzytkownik(UzytkownikEnt uzytkownik) {
		this.uzytkownik = uzytkownik;
		return this;
	}
	public UzytkownicyRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	} 
	
	/**
	 * 
	 */
	public UzytkownicyRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public UzytkownicyRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * 	
	 * @return
	 */
	public UzytkownicyRep AddUzytkownik() {	
		logger.trace(".");
		
		if(this.uzytkownik == null) 
			throw new NullPointerException("Użytkownik nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
			
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	public boolean EditUzytkownik() {
		logger.trace(".");
		
		if(this.uzytkownik == null) 
			throw new NullPointerException("Użytkownik nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.uzytkownik = (UzytkownikEnt) session.merge(this.uzytkownik);
		} catch (Exception e) {	}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return true;	
	}
	
	public boolean DeleteUzytkownik() {
		logger.trace(".");
		
		if(this.uzytkownik == null) 
			throw new NullPointerException("Użytkownik nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.uzytkownik = (UzytkownikEnt) session.merge(this.uzytkownik);
		} catch (Exception e) {	}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<UzytkownikEnt> getUzytkownicy() {
		
		logger.info(".");
		ArrayList<UzytkownikEnt> allUsers = new ArrayList<UzytkownikEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allUsers;
		
		Criteria criteria = session.createCriteria(UzytkownikEnt.class);
		
		criteria.createAlias("pracownik", "p");
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("pracownik.imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pracownik.nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("login", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allUsers = (ArrayList<UzytkownikEnt>) res;
		
		return allUsers;
	}
	

	/**
	 * Potwierdzenie danych logowania
	 * @param user
	 * @param password
	 * @return null gdy nieznaleniono uzytkownika
	 */
	public UzytkownikEnt confirmCredentials(String user, String password) {
		
		logger.info(".Potwierdzenie uprawnień dla " + user);
		
		String hashPass = password; // TODO w przyszłości hasła przygotowane w hash
		
		//this.session = HibernateFactory.getSession();
		if(session == null) // nie można uzyskać sesji
			return null;
		
		Criteria criteria = session.createCriteria(UzytkownikEnt.class);
		
		criteria.add(Restrictions.conjunction()
				.add(Restrictions.eq("login", user))
				.add(Restrictions.eq("haslo", hashPass))
			);

		List<?> res = criteria.list();	
		logger.info(res.size());
		
		if(res.size() == 1)	{
			logger.info(TextTools.oneLine("Znaleziono użytkownika"));
			return (UzytkownikEnt)res.get(0);
		}
		
		logger.info(TextTools.oneLine("Brak takiego użytkownika"));
		return null;
	}
	
	/**
	 * Loguje wskazanego użytkownika z aktualną datą oraz bieżącą sesją
	 * @param uzytkownik
	 * @param sesja
	 * @param timeOut
	 * @return
	 */
	public UzytkownicyRep logIn(UzytkownikEnt uzytkownik, String sesja, long timeOut) {
		
		logger.info(".Rejestrowanie logowania dla "+ uzytkownik.toString());
		
		uzytkownik.setLast_visit(uzytkownik.getTimeout_visit());
		uzytkownik.setTimeout_visit(new java.sql.Timestamp(new Date().getTime() + timeOut));
		uzytkownik.setSession(sesja);
				
		if(session == null)
			return null;
		
		try {
			uzytkownik = (UzytkownikEnt)session.merge(uzytkownik);
		} catch (Exception e) {	}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(uzytkownik);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	
	
	public UzytkownicyRep logOut(UzytkownikEnt uzytkownik) {
		
		logger.info(".");
		
		uzytkownik.setSession(null);
		
		//this.session = HibernateFactory.getSession();
		if(session == null)
			return null;
		
		try {
			uzytkownik = (UzytkownikEnt)session.merge(uzytkownik);
		} catch (Exception e) {	}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	
	/**
	 * Wyciąga użytkownika po jego numerze sesji lub null 
	 * gdy nie znaleziono lub timeout został przekroczony
	 * @param sesja
	 * @return
	 */
	public UzytkownikEnt getUserBySession(String sesja, long timeOut) {
		logger.info(".Get user by session " + sesja);
		
		//this.session = HibernateFactory.getSession();
		if(session == null)
			return null;
		
		Criteria criteria = session.createCriteria(UzytkownikEnt.class);
						
		criteria.add(Restrictions.eq("session", sesja) );

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() == 1) {
			UzytkownikEnt user = (UzytkownikEnt)res.get(0);
			logger.info(".Znaleziono " + user.toString());
			
			//session.evict(user);
			
			if(user.getTimeout_visit().compareTo(new Date()) > 0) {
				return new UzytkownicyRep().updateSession(user, timeOut);
			}
		}
		
		return null;
	}
	
	public UzytkownikEnt updateSession(UzytkownikEnt uzytkownik, long timeOut) {
		
		uzytkownik.setTimeout_visit(new java.sql.Timestamp(new Date().getTime() + timeOut));
		
		//Session ss = HibernateFactory.getSession();
		if(session == null)
			return null;
		
		try {
			uzytkownik = (UzytkownikEnt)session.merge(uzytkownik);
		} catch (Exception e) {	}
		
		logger.info(".podbijanie ");
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return uzytkownik;
	}
	
	public UzytkownicyRep changePassword(UzytkownikEnt uzytkownik, String oldPass, String newPass) {
		
		logger.info(".Miana hasła dla "+ uzytkownik.toString());
		
		if(uzytkownik.getHaslo().compareTo(oldPass) != 0 || newPass == null)
			return null;
		
		if(session == null)
			return null;

		uzytkownik.setHaslo(newPass);
		
		try {
			uzytkownik = (UzytkownikEnt)session.merge(uzytkownik);
		} catch (Exception e) {	}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(uzytkownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		session.flush();
		
		if(uzytkownik.getHaslo().compareTo(newPass) != 0)
			return null;

		return this;
	}
	
}
