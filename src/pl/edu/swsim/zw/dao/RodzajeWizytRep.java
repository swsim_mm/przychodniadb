package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;

public class RodzajeWizytRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private RodzajWizytyEnt rodzajWizyty = null;
	private String SearchText = null;
	
	
	public RodzajeWizytRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public RodzajeWizytRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public RodzajeWizytRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public RodzajeWizytRep setId(long id) {
		this.id = id;
		return this;
	}
	public RodzajeWizytRep setRodzajWizyty(RodzajWizytyEnt rodzajWizyty) {
		this.rodzajWizyty = rodzajWizyty;
		return this;
	}
	public RodzajeWizytRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	} 
	
	/**
	 * 
	 */
	public RodzajeWizytRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public RodzajeWizytRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	
	/**
	 * Dodaje nowy Rodzaj Wizyty
	 * @return
	 */
	public RodzajeWizytRep AddRodzajWizyty() {
		logger.trace(".");
			
		if(this.rodzajWizyty == null) 
			throw new NullPointerException("rodzajWizyty nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
				
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(rodzajWizyty);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edycja Rodzaj Wizyty
	 * @return
	 */
	public RodzajeWizytRep EditRodzajWizyty() {
		logger.trace(".");
		
		if(this.rodzajWizyty == null) 
			throw new NullPointerException("rodzajWizyty nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.rodzajWizyty = (RodzajWizytyEnt) session.merge(this.rodzajWizyty);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(rodzajWizyty);
		
		if(autoTrasaction) 
			this.transaction.commit();

		return this;	
	}
	/**
	 * Usuwa Rodzaj Wizyty
	 * @return
	 */
	public RodzajeWizytRep DeleteRodzajWizyty() {
		logger.trace(".");
		
		if(this.rodzajWizyty == null) 
			throw new NullPointerException("rodzajWizyty nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			this.rodzajWizyty = (RodzajWizytyEnt) session.merge(this.rodzajWizyty);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(rodzajWizyty);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<RodzajWizytyEnt> getRodzajeWizyt() {
		
		logger.info(".");
		ArrayList<RodzajWizytyEnt> allRodzajeWizyt = new ArrayList<RodzajWizytyEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allRodzajeWizyt;
		
		Criteria criteria = session.createCriteria(RodzajWizytyEnt.class);
						
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("nazwa", "%" + this.SearchText + "%"))
				);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allRodzajeWizyt = (ArrayList<RodzajWizytyEnt>) res;
		
		return allRodzajeWizyt;
	}
	
}
