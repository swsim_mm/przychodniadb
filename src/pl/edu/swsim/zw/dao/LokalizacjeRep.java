package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;

public class LokalizacjeRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private String nazwa = null;
	private WojewodztwoEnt wojewodztwo = null;
	private MiastoEnt miasto = null;
	private UlicaEnt ulica = null;
	private String SearchText = null; 
		
	public LokalizacjeRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}
	public LokalizacjeRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public LokalizacjeRep setId(long id) {
		this.id = id;
		return this;
	}
	public LokalizacjeRep setName(String name) {
		this.nazwa = name;
		return this;
	}
	public LokalizacjeRep setWojewodztwo(WojewodztwoEnt wojewodztwo) {
		this.wojewodztwo = wojewodztwo;
		return this;
	}
	public LokalizacjeRep setMiasto(MiastoEnt miasto) {
		this.miasto = miasto;
		return this;
	}
	public LokalizacjeRep setUlica(UlicaEnt ulica) {
		this.ulica = ulica;
		return this;
	}
	
	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	/**
	 * 
	 */
	public LokalizacjeRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public LokalizacjeRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * Dodaje wojewodztwo
	 * @return
	 */
	public LokalizacjeRep AddWojewodztwo() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.wojewodztwo == null) 
			throw new NullPointerException("wojewodztwo nie zostało zdefiniowane");
		
		logger.trace(this.wojewodztwo.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		this.session.saveOrUpdate(this.wojewodztwo);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	/**
	 * Edytuje wojewodztwo
	 * @return
	 */
	public LokalizacjeRep EditWojewodztwo() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.wojewodztwo == null) 
			throw new NullPointerException("wojewodztwo nie zostało zdefiniowane");
		
		logger.trace(this.wojewodztwo.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.wojewodztwo = (WojewodztwoEnt) session.merge(this.wojewodztwo);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.update(this.wojewodztwo);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Usuwa wojewodztwo
	 * @return
	 */
	public LokalizacjeRep DeleteWojewodztwo() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.wojewodztwo == null) 
			throw new NullPointerException("wojewodztwo nie zostało zdefiniowane");
		
		logger.trace(this.wojewodztwo.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.wojewodztwo = (WojewodztwoEnt)session.merge(this.wojewodztwo);
		}
		catch (Exception e) {

		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.delete(this.wojewodztwo);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Dodaje miasto
	 * @return
	 */
	public LokalizacjeRep AddMiasto() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.miasto == null) 
			throw new NullPointerException("Miasto nie zostało zdefiniowane");
		
		logger.trace(this.miasto.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.miasto.getWojewodztwo() != null 
				&& this.miasto.getWojewodztwo().getId() == 0L)
			new LokalizacjeRep().setWojewodztwo(this.miasto.getWojewodztwo()).AddWojewodztwo();
			
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.saveOrUpdate(this.miasto);

		if(autoTrasaction) 
			this.transaction.commit();
		
		
		return this;		
	}
	
	/**
	 * Edytuje miasto
	 * @return
	 */
	public LokalizacjeRep EditMiasto() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.miasto == null) 
			throw new NullPointerException("Miasto nie zostało zdefiniowane");
		
		logger.trace(this.miasto.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.miasto.getWojewodztwo() != null 
				&& this.miasto.getWojewodztwo().getId() == 0L)
			new LokalizacjeRep().setWojewodztwo(this.miasto.getWojewodztwo()).AddWojewodztwo();
		
		try {
			  this.miasto = (MiastoEnt) session.merge(this.miasto);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.update(this.miasto);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Usuwa miasto
	 * @return
	 */
	public LokalizacjeRep DeleteMiasto() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.miasto == null) 
			throw new NullPointerException("Miasto nie zostało zdefiniowane");
		
		logger.trace(this.miasto.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.miasto = (MiastoEnt) session.merge(this.miasto);
		}
		catch (Exception e) { }
		
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.delete(this.miasto);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Dodaje ulicę
	 * @return
	 */
	public LokalizacjeRep AddUlica() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.ulica == null) 
			throw new NullPointerException("Ulica nie została zdefiniowana");
		
		logger.trace(this.ulica.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.ulica.getMiasto() != null 
				&& this.ulica.getMiasto().getId() == 0L)
			new LokalizacjeRep().setMiasto(this.ulica.getMiasto()).AddMiasto();
		
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.saveOrUpdate(this.ulica);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edytuje ulicę
	 * @return
	 */
	public LokalizacjeRep EditUlica() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.ulica == null) 
			throw new NullPointerException("Ulica nie została zdefiniowana");
		
		logger.trace(this.ulica.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.ulica.getMiasto() != null 
				&& this.ulica.getMiasto().getId() == 0L)
			new LokalizacjeRep().setMiasto(this.ulica.getMiasto()).AddMiasto();
		
		try {
			  this.ulica = (UlicaEnt) session.merge(this.ulica);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.update(this.ulica);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Usuwa ulicę
	 * @return
	 */
	public LokalizacjeRep DeleteUlica() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.ulica == null) 
			throw new NullPointerException("Ulica nie została zdefiniowana");
		
		logger.trace(this.ulica.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.ulica = (UlicaEnt) session.merge(this.ulica);
		}
		catch (Exception e) { }
		
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();

		this.session.delete(this.ulica);

		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	
	/**
	 * Wszystkie wojewodztwa, lazy load
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<WojewodztwoEnt> getWojewodztwa() {
		
		logger.info(".");
		ArrayList<WojewodztwoEnt> alWoj = new ArrayList<WojewodztwoEnt>();
		
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			return alWoj;
		
		Criteria criteria = this.session.createCriteria(WojewodztwoEnt.class);
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
		
		/*if(this.nazwa != null)
			criteria.add(Restrictions.eq("name", this.nazwa));*/
		
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);

		List<?> res = criteria.list();		
		
		if(res.size() > 0)
			alWoj = (ArrayList<WojewodztwoEnt>) res;
		
		return alWoj;
	}
	
	/**
	 * Wyszukuje miasta 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<MiastoEnt> getMiasta() {
		logger.info(".");
		ArrayList<MiastoEnt> alMiast = new ArrayList<MiastoEnt>();
		
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			return alMiast;

		Criteria criteria = this.session.createCriteria(MiastoEnt.class);
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
		
		/*if(this.nazwa != null)
			criteria.add(Restrictions.eq("name", this.nazwa));*/
		
		if(this.wojewodztwo != null)
			criteria.add(Restrictions.eq("wojewodztwo.id", this.wojewodztwo.getId()));
		
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction().
					add(Restrictions.like("nazwa", "%" + this.SearchText + "%")).
					add(Restrictions.like("kod_pocztowy", "%" + this.SearchText + "%"))
					);
		}

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		alMiast = (ArrayList<MiastoEnt>)res;
		
		return alMiast;

	}
	
	/**
	 * Wyszukuje ulice we wskazaniu
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UlicaEnt> getUlice() {
		logger.info(".");
		ArrayList<UlicaEnt> alUlice = new ArrayList<UlicaEnt>();
		
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			return alUlice;
		
		Criteria criteria = this.session.createCriteria(UlicaEnt.class);
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
		
		// TODO do wywalenia ?
		if(this.nazwa != null)
			criteria.add(Restrictions.eq("name", this.nazwa));
		
		if(this.miasto != null)
			criteria.add(Restrictions.eq("miasto.id", this.miasto.getId()));
		
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);

		if(this.SearchText != null) {
			criteria.add(Restrictions.like("nazwa", "%" + this.SearchText + "%"));
		}
		
		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		alUlice = (ArrayList<UlicaEnt>)res;
		
		return alUlice;

	}
	
	/**
	 * Wyszukuje pełną lokalizację
	 * @param miasto
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UlicaEnt> getLokalizacje() {
		logger.info(".");
		ArrayList<UlicaEnt> alUlice = new ArrayList<UlicaEnt>();
		
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			return alUlice;
		
		Criteria criteria = this.session.createCriteria(UlicaEnt.class);
		criteria.createAlias("miasto", "m");
		criteria.createAlias("miasto.wojewodztwo", "w");
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
		
		if(this.nazwa != null)
			criteria.add(Restrictions.eq("nazwa", this.nazwa));
		
		if(this.wojewodztwo != null)
			criteria.add(Restrictions.eq("wojewodztwo", this.wojewodztwo));
		
		if(this.miasto != null)
			criteria.add(Restrictions.eq("miasto", this.miasto));
		
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction().
					add(Restrictions.like("nazwa", "%" + this.SearchText + "%")).
					add(Restrictions.like("m.nazwa", "%" + this.SearchText + "%")).
					add(Restrictions.like("w.nazwa", "%" + this.SearchText + "%"))
					);
		}
		

		List<?> res = criteria.list();	
		
		logger.trace(res.size());
		
		if(res.size() > 0)
			alUlice = (ArrayList<UlicaEnt>)res;
		
		return alUlice;

	}
	

}
