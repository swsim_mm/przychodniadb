package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.entities.PracownikEnt;

public class PracownicyRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private PracownikEnt pracownik = null;
	private String SearchText = null; 
	private int rodzajPracownika = PracownikEnt.TYP_DOWOLNY;

	public PracownicyRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public PracownicyRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public PracownicyRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public PracownicyRep setId(long id) {
		this.id = id;
		return this;
	}
	public PracownicyRep setPracownik(PracownikEnt pracownik) {
		this.pracownik = pracownik;
		return this;
	}
	public PracownicyRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}
	public PracownicyRep setRodzajPracownika(int rodzajPracownika) {
		this.rodzajPracownika = rodzajPracownika;
		return this;
	}
	
	/**
	 * 
	 */
	public PracownicyRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public PracownicyRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * Dodaje nowego pracownika	
	 * @return
	 */
	public PracownicyRep AddPracownik() {
		logger.trace(TextTools.twoLine(""));
				
		if(this.pracownik == null) 
			throw new NullPointerException("pracownik nie został zdefiniowany");
		
		logger.trace(this.pracownik.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.pracownik.getUlica() != null 
				&& this.pracownik.getUlica().getId() == 0L)
			new LokalizacjeRep().setUlica(this.pracownik.getUlica()).AddUlica().commit();
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(pracownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edytuje pracownika
	 * @return
	 * @fix naprawienie presistent oraz duplikatu 
	 */
	public PracownicyRep EditPracownik() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.pracownik == null) 
			throw new NullPointerException("pracownik nie został zdefiniowany");
		
		logger.trace(this.pracownik.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.pracownik.getUlica() != null 
				&& this.pracownik.getUlica().getId() == 0L)
			new LokalizacjeRep().setUlica(this.pracownik.getUlica()).AddUlica().commit();
		
		try {
			  this.pracownik = (PracownikEnt) session.merge(this.pracownik);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(pracownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	/**
	 * 
	 * @return
	 */
	public PracownicyRep DeletePracownik() {
		logger.trace(TextTools.twoLine(""));
		
		if(this.pracownik == null) 
			throw new NullPointerException("pracownik nie został zdefiniowany");
		
		logger.trace(this.pracownik.toString());
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.pracownik = (PracownikEnt) session.merge(this.pracownik);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(pracownik);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<PracownikEnt> getPracownicy() {
		
		logger.info(".");
		ArrayList<PracownikEnt> allPrac = new ArrayList<PracownikEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allPrac;
		
		Criteria criteria = session.createCriteria(PracownikEnt.class);
				
		criteria.createAlias("ulica", "u");
		criteria.createAlias("ulica.miasto", "m");
		criteria.createAlias("ulica.miasto.wojewodztwo", "w");
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pesel", "%" + this.SearchText + "%"))
					.add(Restrictions.like("u.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("m.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("w.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("dodatkowe", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pwz", "%" + this.SearchText + "%"))
				);
		}
		
		if(this.rodzajPracownika == PracownikEnt.TYP_LEKARZ)
			criteria.add(Restrictions.eq("lekarz", true));
		else if(this.rodzajPracownika == PracownikEnt.TYP_INNY)
			criteria.add(Restrictions.eq("lekarz", false));

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allPrac = (ArrayList<PracownikEnt>) res;
		
		return allPrac;
	}

}