package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.RozpoznanieEnt;

public class RozpoznaniaRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private RozpoznanieEnt rozpoznanie = null;
	private String SearchText = null; 
	
	private PacjentEnt pacjent = null;
	

	public RozpoznaniaRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public RozpoznaniaRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public RozpoznaniaRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public RozpoznaniaRep setId(long id) {
		this.id = id;
		return this;
	}
	public RozpoznaniaRep setRozpoznanie(RozpoznanieEnt rozpoznanie) {
		this.rozpoznanie = rozpoznanie;
		return this;
	}
	public RozpoznaniaRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}
	
	public RozpoznaniaRep setPacjent(PacjentEnt pacjent) {
		this.pacjent = pacjent;
		return this;
	}
	
	/**
	 * 
	 */
	public RozpoznaniaRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public RozpoznaniaRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * Dodaje nowy zabieg
	 * @return
	 */
	public RozpoznaniaRep AddRozpoznanie() {
		logger.trace(".");
			
		if(this.rozpoznanie == null) 
			throw new NullPointerException("rozpoznanie nie zostało zdefiniowane");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
	
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(rozpoznanie);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * Edytuje zabieg
	 * @return
	 */
	public RozpoznaniaRep EditRozpoznanie() {
		logger.trace(".");
		
		if(this.rozpoznanie == null) 
			throw new NullPointerException("rozpoznanie nie zostało zdefiniowane");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.rozpoznanie = (RozpoznanieEnt) session.merge(this.rozpoznanie);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(rozpoznanie);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	/**
	 * 
	 * @return
	 */
	public RozpoznaniaRep DeleteRozpoznanie() {
		logger.trace(".");
		
		if(this.rozpoznanie == null) 
			throw new NullPointerException("rozpoznanie nie zostało zdefiniowane");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.rozpoznanie = (RozpoznanieEnt) session.merge(this.rozpoznanie);
		}
		catch (Exception e) { }
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(rozpoznanie);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<RozpoznanieEnt> getRozpoznania() {
		
		logger.info(".");
		ArrayList<RozpoznanieEnt> allRozpoznania = new ArrayList<RozpoznanieEnt>();
		
		this.session = HibernateFactory.getSession();
		if(session == null)
			return allRozpoznania;
		
		Criteria criteria = session.createCriteria(RozpoznanieEnt.class);
		
		criteria.createAlias("wizyta", "w");
		criteria.createAlias("wizyta.pacjent", "pa");
		criteria.createAlias("wizyta.pracownik", "pr");
		criteria.createAlias("choroba", "c");
		
						
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {

			
			criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("opis", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("w.dodatkowe", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("c.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("c.opis", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("pa.imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.pesel", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.ulica.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.ulica.miasto.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pa.ulica.miasto.wojewodztwo.nazwa", "%" + this.SearchText + "%"))
					
					.add(Restrictions.like("pr.imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pr.nazwisko", "%" + this.SearchText + "%"))
				);
		}
		
		if(this.pacjent != null) {
			criteria.add(Restrictions.eq("pa.id", this.pacjent.getId()));
		}
		

		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			allRozpoznania = (ArrayList<RozpoznanieEnt>) res;
		
		return allRozpoznania;
	}


}
