package pl.edu.swsim.zw.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;

public class PacjenciRep {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	private Session session = null;
	private Transaction transaction = null;
	private boolean autoTrasaction = true;
	
	private int limit = -1;
	private int offset = -1;
	
	private ArrayList<Order> orderBy = null;
	private long id = -1;
	private PacjentEnt pacjent = null;
	private PracownikEnt pracownik = null;

	private String SearchText = null; 
	
	public PacjenciRep setLimit(int limit) {
		this.limit = limit;
		return this;
	}
	public PacjenciRep setOffset(int offset) {
		this.offset = offset;
		return this;
	}
	public PacjenciRep setOrderBy(ArrayList<Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	public PacjenciRep setId(long id) {
		this.id = id;
		return this;
	}
	public PacjenciRep setPacjent(PacjentEnt pacjent) {
		this.pacjent = pacjent;
		return this;
	}
	public PacjenciRep setSearchText(String searchText) {
		SearchText = searchText;
		return this;
	}
	public void setPracownik(PracownikEnt pracownik) {
		this.pracownik = pracownik;
	}

	/**
	 * 
	 */
	public PacjenciRep() {
		this.session = HibernateFactory.getSession();
		if(this.session == null)
			logger.fatal("Błąd pobieranie sesji dostępu do sql");
	};
	
	public PacjenciRep(Session session) {
		this.session = session;
		autoTrasaction = false;
	};
	
	public void commit() {
		if(!autoTrasaction)
			this.transaction.commit();
	}
	
	/**
	 * 
	 * @return
	 */
	public PacjenciRep AddPacjent() {
		logger.trace(".");
		
		if(this.pacjent == null) 
			throw new NullPointerException("pacjent nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.pacjent.getUlica() != null 
				&& this.pacjent.getUlica().getId() == 0L)
			new LokalizacjeRep().setUlica(this.pacjent.getUlica()).AddUlica();
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.saveOrUpdate(pacjent);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;		
	}
	
	/**
	 * 
	 * @return
	 */
	public PacjenciRep EditPacjent() {
		logger.trace(".");
		
		if(this.pacjent == null) 
			throw new NullPointerException("pacjent nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		if(this.pacjent.getUlica() != null 
				&& this.pacjent.getUlica().getId() == 0L)
			new LokalizacjeRep().setUlica(this.pacjent.getUlica()).AddUlica();
		
		try {
			  this.pacjent = (PacjentEnt) session.merge(this.pacjent);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.update(pacjent);
		
		if(autoTrasaction) 
			this.transaction.commit();
		
		return this;	
	}
	
	/**
	 * 
	 * @return
	 */
	public PacjenciRep DeletePacjent() {
		logger.trace(".");
		
		if(this.pacjent == null) 
			throw new NullPointerException("pacjent nie został zdefiniowany");
		
		if(this.session == null)
			throw new NullPointerException("Błąd sesji");
		
		try {
			  this.pacjent = (PacjentEnt) session.merge(this.pacjent);
		}
		catch (Exception e) {
		}
		
		if(autoTrasaction) 
			this.transaction = session.beginTransaction();
		
		session.delete(pacjent);
		
		if(autoTrasaction) 
			this.transaction.commit();

		return this;
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<PacjentEnt> getPacjenci() {
		
		logger.info(".");
		ArrayList<PacjentEnt> alPac = new ArrayList<PacjentEnt>();
		
		Session session = HibernateFactory.getSession();
		if(session == null)
			return alPac;
		
		Criteria criteria = session.createCriteria(PacjentEnt.class);	
		
		criteria.createAlias("ulica", "u");
		criteria.createAlias("ulica.miasto", "m");
		criteria.createAlias("ulica.miasto.wojewodztwo", "w");
		criteria.createAlias("pracownik", "p");
		
		if(this.id > 0)
			criteria.add(Restrictions.eq("id", this.id));
				
		if(this.orderBy != null && this.orderBy.size() > 0)
			for(Order ord : this.orderBy)
				criteria.addOrder(ord);
	
		if(this.limit > -1) 
			criteria.setMaxResults(this.limit);
		
		if(this.offset > -1) 
			criteria.setFirstResult(this.offset);
		
		
		if(this.SearchText != null) {
			
			String[] tokens = this.SearchText.split("[^A-Za-z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]+");
			//A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ
			if(tokens.length == 1) {
				criteria.add(Restrictions.disjunction()
					.add(Restrictions.like("imie", "%" + this.SearchText + "%"))
					.add(Restrictions.like("nazwisko", "%" + this.SearchText + "%"))
					.add(Restrictions.like("pesel", "%" + this.SearchText + "%"))
					.add(Restrictions.like("u.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("m.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("w.nazwa", "%" + this.SearchText + "%"))
					.add(Restrictions.like("dodatkowe", "%" + this.SearchText + "%"))
				);
			} else {
				logger.trace("searchtext " + tokens.length + "");
				
				String search  = "";
				for(int i=0; i< tokens.length; i++) {
					search += tokens[i] + ", ";
	 			}
				
				logger.trace("searchtext " + search);
				
				Disjunction ds = Restrictions.disjunction();
								
				if(tokens.length <=2) {
					ds.add(Restrictions.like("imie", "%" + tokens[0] + "%"));
					ds.add(Restrictions.like("nazwisko", "%" +tokens[1] + "%"));
				}
				

				criteria.add(ds);
				
				// TODO POLSKIE ZNAKI !!!!
			}

		}
		
		if(this.pracownik != null) {
			criteria.add(Restrictions.eq("pracownik.id", pracownik.getId()));
		}
		
		List<?> res = criteria.list();	
		
		logger.info(res.size());
		
		if(res.size() > 0)		
			alPac = (ArrayList<PacjentEnt>) res;
		
		return alPac;
	}
}
