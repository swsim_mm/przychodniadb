
--
-- Zrzut danych tabeli `wojewodztwa`
--

INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(1, 'dolnośląskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(2, 'kujawsko-pomorskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(3, 'lubelskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(4, 'lubuskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(5, 'łódzkie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(6, 'małopolskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(7, 'mazowieckie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(8, 'opolskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(9, 'podkarpackie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(10, 'podlaskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(11, 'pomorskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(12, 'śląskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(13, 'świętokrzyskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(14, 'warmińsko-mazurskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(15, 'wielkopolskie');
INSERT INTO `wojewodztwa` (`id`, `nazwa`) VALUES(16, 'zachodniopomorskie');

--
-- Zrzut danych tabeli `miasta`
--

INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(1, '41-940', 'Piekary Śląskie', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(2, '41-943', 'Piekary Śląskie', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(3, '41-907', 'Bytom', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(4, '41-900', 'Bytom', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(5, '42-200', 'Częstochowa', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(6, '50-001', 'Wrocław', 1);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(7, '81-002', 'Gdynia', 10);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(8, '70-001', 'Szczecin', 16);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(9, '41-500', 'Chorzów', 12);
INSERT INTO `miasta` (`id`, `kod_pocztowy`, `nazwa`, `wojewodztwoid`) VALUES(10, '81-000', 'Gdynia', 11);

--
-- Zrzut danych tabeli `ulice`
--

INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(1, 'Damrota', 1);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(2, 'Powstańców Śląskich', 1);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(3, 'Bytomska', 1);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(4, 'Bytomska1', 1);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(5, 'Wesoła', 4);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(6, 'Gallusa', 3);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(7, 'Konstytucji 3 Maja', 2);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(8, 'Mickiewicza', 4);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(9, 'Rozkopana', 5);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(10, 'Zaborców', 5);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(11, 'Prawdziwej Wiary', 5);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(12, 'Łabędzi', 5);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(13, 'Zwyczajna', 5);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(14, 'Nadzwyczajna', 8);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(15, 'Szkolna', 9);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(17, 'Morska', 10);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(18, 'Konstytucji 3 Maja ', 2);
INSERT INTO `ulice` (`id`, `nazwa`, `miastoid`) VALUES(20, 'Dawida', 6);


--
-- Zrzut danych tabeli `choroby`
--

INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(1, 'Przeziębienie/Grypa', '', b'0');
INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(2, 'Cukrzyca typu 1', 'Autoimmunologiczny proces chorobowy cukrzycy insulinoniezależnej', b'1');
INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(3, 'Cukrzyca typu 2', 'Choroba metaboliczna cukrzycy insulinoniezależnej', b'1');
INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(4, 'Zapalenie płuc', '', b'0');
INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(5, 'Zapalenie krtani', '', b'0');
INSERT INTO `choroby` (`id`, `nazwa`, `opis`, `przewlekla`) VALUES(6, 'Próchnica zebów', '', b'0');


--
-- Zrzut danych tabeli `gabinety`
--

INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(1, 'Ogólny, wizyty', '2');
INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(2, 'Zabiegowy mały przy wizytach', '2a');
INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(3, 'Zabiegowy, duży', '6');
INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(4, 'Wizyty dzieci', '7');
INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(5, 'Zabiegi bezinwazyjne, dla dzieci', '8');
INSERT INTO `gabinety` (`id`, `nazwa`, `numer`) VALUES(6, 'Dentystyczny', '666');


--
-- Zrzut danych tabeli `pracownicy`
--
INSERT INTO `pracownicy` (`id`, `budynek`, `dodatkowe`, `imie`, `lekarz`, `mieszkanie`, `nazwisko`, `pesel`, `pwz`, `telefon`, `ulicaid`) VALUES(1, '', NULL, 'Administrator', b'0', '', '', '', NULL, NULL, NULL);
INSERT INTO `pracownicy` (`id`, `budynek`, `dodatkowe`, `imie`, `lekarz`, `mieszkanie`, `nazwisko`, `pesel`, `pwz`, `telefon`, `ulicaid`) VALUES(2, '44/2', '', 'Michał', 1, '17', 'Żak', '87112508334', NULL, '796161604', 7);
INSERT INTO `pracownicy` (`id`, `budynek`, `dodatkowe`, `imie`, `lekarz`, `mieszkanie`, `nazwisko`, `pesel`, `pwz`, `telefon`, `ulicaid`) VALUES(3, '2', '', 'Jan', 1, '1', 'Kowalski', '64122117811', '5425740', '', 14);
INSERT INTO `pracownicy` (`id`, `budynek`, `dodatkowe`, `imie`, `lekarz`, `mieszkanie`, `nazwisko`, `pesel`, `pwz`, `telefon`, `ulicaid`) VALUES(4, '15', '', 'Milena', 1, '37', 'Larysz', '89050809968', NULL, '', 6);
INSERT INTO `pracownicy` (`id`, `budynek`, `dodatkowe`, `imie`, `lekarz`, `mieszkanie`, `nazwisko`, `pesel`, `pwz`, `telefon`, `ulicaid`) VALUES(5, '', '', 'test', 1, '', 'testowy', '89050809012', NULL, '', 3);

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `haslo`, `last_visit`, `login`, `session`, `timeout_visit`, `pracownikid`, `administrator`) VALUES(1, '21232f297a57a5a743894a0e4a801fc3', '2015-06-11 00:42:57', 'admin', '', '2015-06-11 13:24:08', 1, b'1');
INSERT INTO `uzytkownicy` (`id`, `haslo`, `last_visit`, `login`, `session`, `timeout_visit`, `pracownikid`, `administrator`) VALUES(2, '1a1dc91c907325c69271ddf0c944bc72', '2015-05-22 23:10:12', 'mike', '', '2015-05-22 23:34:06', 2, b'0');

--
-- Zrzut danych tabeli `pacjenci`
--

INSERT INTO `pacjenci` (`id`, `budynek`, `dodatkowe`, `imie`, `karta_nfz`, `mieszkanie`, `nazwisko`, `pesel`, `telefon`, `pracownikid`, `ulicaid`) VALUES(1, '1a', '', 'Andrzej', NULL, '5', 'Znachor', '33030905450', '78997789', 2, 8);
INSERT INTO `pacjenci` (`id`, `budynek`, `dodatkowe`, `imie`, `karta_nfz`, `mieszkanie`, `nazwisko`, `pesel`, `telefon`, `pracownikid`, `ulicaid`) VALUES(2, '456', '', 'Paweł', NULL, '1', 'Nagłowęupadł', '92120112453', '050099900', 3, 3);
INSERT INTO `pacjenci` (`id`, `budynek`, `dodatkowe`, `imie`, `karta_nfz`, `mieszkanie`, `nazwisko`, `pesel`, `telefon`, `pracownikid`, `ulicaid`) VALUES(3, '1', 'Pacjent jest bardzo niespokojny', 'Maciej', NULL, '2', 'Niespokojny', '92120112453', '', 2, 20);

--
-- Zrzut danych tabeli `rodzaje_wizyt`
--

INSERT INTO `rodzaje_wizyt` (`id`, `czastrwania`, `nazwa`, `gabinetid`) VALUES(1, '00:15:27', 'Wizyta kontrolna (zdrowy)', 1);
INSERT INTO `rodzaje_wizyt` (`id`, `czastrwania`, `nazwa`, `gabinetid`) VALUES(2, '00:20:09', 'Cukrzycy i diabetycy', 1);
INSERT INTO `rodzaje_wizyt` (`id`, `czastrwania`, `nazwa`, `gabinetid`) VALUES(3, '00:25:20', 'Dzieci chore', 4);
INSERT INTO `rodzaje_wizyt` (`id`, `czastrwania`, `nazwa`, `gabinetid`) VALUES(4, '00:20:33', 'Dzieci zdrowe lub kontrolne', 4);
INSERT INTO `rodzaje_wizyt` (`id`, `czastrwania`, `nazwa`, `gabinetid`) VALUES(5, '00:40:00', 'Ból zebów', 6);

--
-- Zrzut danych tabeli `rodzaje_zabiegow`
--

INSERT INTO `rodzaje_zabiegow` (`id`, `cena`, `czastrwania`, `nazwa`, `opis`, `punkty`, `refundacja`, `rodzaj`, `skierowanie`, `gabinetid`) VALUES(1, 8000, '00:45:00', 'Usunięcie zęba', '', 5, b'1', 'Dentystyczny', b'0', 6);
INSERT INTO `rodzaje_zabiegow` (`id`, `cena`, `czastrwania`, `nazwa`, `opis`, `punkty`, `refundacja`, `rodzaj`, `skierowanie`, `gabinetid`) VALUES(2, 2000, '00:10:00', 'Szczepienie przeciw grypie', '', 2, b'1', '', b'0', 2);

