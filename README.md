# README #

Przychodnia - projekt na zaliczenie Projektowanie Aplikacji Bazodanowych

Część BAZODANOWA

> Elementy przygotowane proszę oznaczać
> + dla elementu 100% wykonanego - jeśli nie jest gotowy
> za znakiem można umieszczać % ukończenia (+ 20% nazwa)

---------------------------------------------
### TODO List czyli projekt aplikacji ###

+ Połaczenie Hibernate
+ Baza danych
+ Okno i ułożenie (może poprawimy później)
---------------------------------------------
* Słowniki
	* Lokalizacje (ulice, miasta, wojewodztwa)
		+ Repozytorium
		+ Lista
		+ Interfejs-Dialog (dodawanie, edycja usuwanie)
	* Gabinety
		+ Repozytorium 
		+ Lista
		+ Interfejs-Dialog
	* Choroby
		+ Repozytorium 
		+ Lista (jako słownik)
		+ Interfejs-Dialog (dodaj usun itd)
	* Rodzaje wizyt
		+ Repozytorium 
		+ Lista (jako słownik)
		+ Interfejs-Dialog (dodaj usun edycja)
	* Rodzaje zabiegów
		+ Repozytorium -> razem z zabiegi
		+ Lista 
		+ Interfejs-Dialog
-------------------------------------------	
* Pracownicy
	+ Repository
	+ Lista
	+ interfejs Dodawanie, edycja, usuwanie
	- grupy
	
* Pacjenci
	+ Repository
	+ Lista
	+ Interfejs Dodawanie, edycja, usuwanie
	+ relacje z tabelami
-------------------------------------------
* Użytkownicy
	+ Repository
	+ Lista
	+ Interfejs Dodawanie, edycja, usuwanie
	+ relacje z tabelami
-------------------------------------------		
* Terminy
	+ Repozytorium
	+ Lista
	+ Interfejs-Dialog
	+ Prosta edycja - określenie i ulepszenie (stand alone wywołanie)
	
* Rozpoznania
	+ Repozytorium
	+ Lista
	+ Interfejs-Dialog
	
### TODO Dodatkowe jeśli starczy czasu ###
- wyszukiwanie używając przecinka
- naprawienie cascade żeby nie usuwać słowników w użyciu